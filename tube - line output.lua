-- ######################################################
--               Smithy output tube 
--         by Guill4um on december, 05th of 2020
-- ######################################################

-- this tube check if the machine is free to ask more material
-- when needed 

local LINE_NB = 1
local IN_LINE = "blue"
local OUT = "black"

if event.type == "item"  then
    if event.pin.name == IN_LINE then
        interrupt(10,"FREE_LINE")
    end
elseif event.type == "interrupt" then
    if event.iid == "FREE_LINE" then
        digiline_send("process","SEND_NEXT"..tostring(LINE_NB))
    end
end
return OUT